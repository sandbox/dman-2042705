Local Tasks as Menu
====================

Allows an admin to shift certain 'local task' tabs into proper menu structure.

This allows for more control of positioning and theming, as well as order
and label management.

Initially, this can be used to promote 'tabs 'on user account pages 
(under /user/* ) into the 'user-menu'.

Additional parts of the admin interface that use MENU_LOCAL_TASK can also
be promoted, though some custom paths may need additional work to support.


Usage
-------------

When first installed, the 'user' local tabs will be shifted into the user-menu
where they can then be managed.

When this happens the menu paths (eg user/{uid}/edit) continue to work, but 
they will no longer show up as 'local tabs'. Instead, you MUST now display the 
'user-menu' as a block somewhere in your theme regions. 
You can manage the items and theming from then on as normal.

Enabling this module WITHOUT then showing the target menu block may make
some functionality inaccessible.

Configuration
-------------

To add more local tabs to the world of full menu items, the variable
  local_tasks_as_menu_rewrites
Should be set.

This can be done in settings.php $conf values, eg:

  $conf['local_tasks_as_menu_rewrites']['node/%node/'] = array(
    'path-replacement' => 'node/%node_nid_optional/',
    'type' => MENU_NORMAL_ITEM,
  );

.. Would remove all local tabs (edit, revisions etc) from node pages.
If you do this, you will have to make those actions available another way.


Background
----------

I'd hoped that this would be as simple as a hook_menu_alter
that changed MENU_LOCAL_TASK to MENU_NORMAL_ITEM, but it turns out there
is an extra trick needed.

Helpfully, http://devbee.com/2009/04/moving-tabs-main-menu
by Harry Slaughter https://drupal.org/user/28233 had the answer.
This module is an evolution of that blog post.

In retrospect, the hook_menu docs provide some clues, but after reading it
a few times, I still wouldn't have guessed that we needed "Argument Wildcards"

@see https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_menu/7

Alternatives
------------

Other implementations are suggested at 
http://drupal.stackexchange.com/questions/17471/how-can-menu-tabs-be-relocated-to-another-region
and include local_tasks_block.module , panels_everywhere and blockify.
Each of those just shifts the rendering around the page 
 - which is half the job, but doesn't help a lot with the rest.
